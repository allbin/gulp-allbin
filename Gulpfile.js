let gulp = require('gulp4');
let helpers = require('./index.js');

gulp.task('release:patch', helpers.tagAndPush(["package.json", "index.js"], "patch"));
gulp.task('release:minor', helpers.tagAndPush(["package.json", "index.js"], "minor"));
gulp.task('release:major', helpers.tagAndPush(["package.json", "index.js"], "major"));
let gulp = require('gulp4');
let del = require('del');
let bump = require('gulp-bump');
let eslint = require('gulp-eslint');
let babel = require('gulp-babel');
let exec = require('child_process').exec;
let fs = require('fs');

let execPromise = (cmd) => {
    return new Promise((resolve, reject) => {
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                return reject(new Error(cmd + ": " + stderr));
            }
            return resolve();
        });
    });
};

let paths = {
    scripts: ['src/**/*.js', 'src/**/*.jsx'],
    clean: ['build', 'dist']
};


// ---- preparations ----
gulp.task('clean', () => {
    return del(paths.clean);
});

gulp.task('lint', () => {
    return gulp.src(paths.scripts)
        .pipe(eslint({ configFile: "./.eslintrc.js" }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});


// ---- build ----
gulp.task('build:prep', gulp.series('clean', 'lint'));

gulp.task('build', gulp.series('build:prep', () => {
    return gulp.src(paths.scripts)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'));
}));

gulp.task('bump:patch', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump())
        .pipe(gulp.dest('.'));
});
gulp.task('bump:minor', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'minor'}))
        .pipe(gulp.dest('.'));
});
gulp.task('bump:major', (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'major'}))
        .pipe(gulp.dest('.'));
});


function addFilesCommitTagPush(cb, files = null) {
    if (!files || !Array.isArray(files)) {
        return Promise.reject("Files property is required to be an array of files to 'git add'.");
    }

    let version = "";

    return Promise.resolve().then(() => {
        version = JSON.parse(fs.readFileSync('package.json')).version;
    }).then(() => {
        return execPromise('git add ' + files.join(' '));
    }).then(() => {
        console.log("Added files '" + files.join(' ') + "' to git.");
        return execPromise('git commit -m "Release v' + version + '"');
    }).then(() => {
        return execPromise('git tag v' + version);
    }).then(() => {
        console.log("Commit and tagged 'v" + version + "'.");
        return execPromise('git push && git push --tags');
    }).then(() => {
        console.log("Pushed.");
        cb();
    }).catch((err) => {
        cb(err);
    });
}


function tagDevAndPush(cb, files = null) {
    if (!files || !Array.isArray(files)) {
        return Promise.reject("Files property is required to be an array of files to 'git add'.");
    }

    try {
        Promise.resolve()
            .then(() => { return execPromise('git add ' + files.join(' ')); })
            .then(() => { return execPromise('git commit -m "Release dev"'); })
            .catch((err) => {
                //This empty catch is here because git commit will fail if there
                //were no changes but you still want to do a dev release.
            })
            .then(() => { return execPromise('git tag --force dev'); })
            .then(() => { return execPromise('git push && git push --tags -f'); })
            .then(() => { return cb(); });

    } catch (err) {
        return cb(err);
    }
}


function pushBumpAndTag(cb) {
    try {
        let pkg = JSON.parse(fs.readFileSync('package.json'));

        Promise.resolve()
            .then(() => { return execPromise('git add package.json dist'); })
            .then(() => { return execPromise('git commit -m "Release v' + pkg.version + '"'); })
            .then(() => { return execPromise('git tag v' + pkg.version); })
            .then(() => { return execPromise('git push && git push --tags'); })
            .then(() => { return cb(); });

    } catch (err) {
        return cb(err);
    }
}

function buildBumpPatchPush(new_paths = null) {
    if (new_paths) {
        setPaths(new_paths);
    }
    return gulp.series('bump:patch', 'build', pushBumpAndTag);
}
function buildBumpMinorPush(new_paths = null) {
    if (new_paths) {
        setPaths(new_paths);
    }
    return gulp.series('bump:minor', 'build', pushBumpAndTag);
}
function buildBumpMajorPush(new_paths = null) {
    if (new_paths) {
        setPaths(new_paths);
    }
    return gulp.series('bump:major', 'build', pushBumpAndTag);
}

function tagAndPush(files = null, bump = "patch") {
    //Defaults to only adding package.json to the commit.
    files = files || ["package.json"];

    if (bump === "dev") {
        return gulp.series((cb) => { tagDevAndPush(cb, files); });
    } else if (bump === "major") {
        return gulp.series('bump:major', (cb) => { addFilesCommitTagPush(cb, files); });
    } else if (bump === "minor") {
        return gulp.series('bump:minor', (cb) => { addFilesCommitTagPush(cb, files); });
    }
    return gulp.series('bump:patch', (cb) => { addFilesCommitTagPush(cb, files); });
}

function setPaths(new_paths) {
    paths = Object.assign({}, paths, new_paths);
}

module.exports = {
    buildBumpPatchPush: buildBumpPatchPush,
    buildBumpMinorPush: buildBumpMinorPush,
    buildBumpMajorPush: buildBumpMajorPush,
    tagAndPush: tagAndPush,
    setPaths: setPaths
};
